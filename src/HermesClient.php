<?php

namespace GEx\HermesClient;

class HermesClient {

	private $username;
	private $password;
	private $host;
	private $protocol;
	private $token;

	/**
	 * 
	 * @param string $username
	 * @param string $password
	 * @param string $host
	 */
	public function __construct($username, $password, $host = 'hermes.exp.mx', $protocol = 'http') {
		$this->username = $username;
		$this->password = $password;
		$this->host = $host;
		$this->protocol = $protocol;
		$this->token = null;
	}

	private function executeRequest($requestUrl, $requestMap) {
		$json = json_encode($requestMap);
		$options = array(
			CURLOPT_URL => $requestUrl,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS => $json,
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/json',
				'X-Auth-Token: ' . $this->token
			)
		);

		$ch = curl_init();
		curl_setopt_array($ch, $options);

		$response = curl_exec($ch);

		$error = curl_error($ch);
		$result = array('body' => '', 'curl_error' => '', 'status_code' => '');

		if ($error != "") {
			$result['curl_error'] = $error;
			return $result;
		}

		$result['body'] = json_decode($response, true);
		$result['status_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		return $result;
	}

	private function ensureLogin() {
		if ($this->token == null) {
			$requestUrl = $this->protocol . '://' . $this->host . '/login';

			$requestMap = array(
				"username" => $this->username,
				"password" => $this->password
			);

			$result = $this->executeRequest($requestUrl, $requestMap);

			$this->token = $result['body']['token'];
		}
	}

	public function publishMessage($productId, array $params, $development = false) {
		$this->ensureLogin();

		$requestUrl = $this->protocol . '://' . $this->host . '/messaging/send';

		$requestMap = array(
			"productId" => $productId,
			"message" => $params,
			"development" => $development
		);

		return $this->executeRequest($requestUrl, $requestMap);
	}

}
