<?php

use GEx\HermesClient\HermesClient;

describe("The client", function () {
	let('client', new HermesClient('plataforma', 'password', 'hermes.stage.exp.mx'));
	
	
	describe('sends messages', function () {
		it('publishMessage() is able to send a message to an existing product', function ($client) {
			$message = array(
				"message" => "Hola mundo"
			);
			
			$response = $client->publishMessage("d31daac4-39f1-4c29-8132-da3bad61eb67", $message, true);
			
			expect($response['status_code'])->toBe(200);
		});
	});
});
