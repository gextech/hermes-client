# Hermes Client

This is a client for Hermes notifier, it is a very simple service which allows
to send PUSH notifications to Android and iOS.

## Usage

Install by adding to your composer.json:

```
#!json
{
    "repositories": [
        {
            "url": "git@bitbucket.org:gextech/hermes-client.git",
            "type": "git"
        }
    ],
    "require": {
        "gex/hermes-client": "dev-master@dev"
    }
}
```

After that you can create your client and send a message like this:

```
#!php
<?php
use \GEx\HermesClient\HermesClient;

$client = new HermesClient($username, $password, 'hermes.stage.exp.mx');

$development = true;
$message = array(
    "message" => "El mensaje",
    "sound" => "sonidito.wav",
    "meta" => array(
        "id" => 17
    )
);
$client->publishMessage($siteToken, $message, $development);

```

